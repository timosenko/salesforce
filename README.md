# Magento SalesForce Integration #

The following repository contains a fresh Magento CE installation with some basic SalesForce integrations.

### Current Extensions: ###
* SfBasic - Provides basic interface for connection.
* SfContacts - Synchronises the created\updated Customers in Magento with SalesForce Accounts & Contacts. Basically, creates an Account for each Customer and then assigns a new Contact to that Account.

### How to connect ###
* Admin -> System -> Configuration -> Scandi -> Salesforce 
* Enter the credentials (can ask matthew@scandiweb.com for Security Token)
* Save Config