<?php
/**
 * @category    Scandi
 * @package    Scandi_SfContacts
 * @author    Matthew <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 */
class Scandi_SfContacts_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Function to import all contacts as customers into Magento store
     * @return boolean
     */
    public function importAllCustomers()
    {
        $query = "SELECT * from Contact";
        $customers = Mage::helper('scandi_sfbasic')->sfQuery($query);

        $websiteId = Mage::app()->getWebsite()->getId();
        $store = Mage::app()->getStore();

        foreach ($customers as $item){
            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId($websiteId)
                ->setStore($store)
                ->setFirstname($item->Firstname)
                ->setLastname($item->Lastname)
                ->setEmail($item->Email)
                ->setPassword(rand(time()).sha1($item->id));

            try{
                $customer->save();
            }
            catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
            }
        }
        return true;
    }

    /**
     * Function which creates a new Account for your SalesForce organization, then assigns it to a new Contact (customer)
     * @return boolean
     */
    public function exportCustomer($customer)
    {
        $connection = Mage::helper('scandi_sfbasic')->sfConnect();
        $accountFields = array (
            'Name' => $customer->getFirstname(),
        );

        try {
            $sObject = new SObject();
            $sObject->fields = $accountFields;
            $sObject->type = 'Account';
            $account = $connection->create(array($sObject));

            $customerFields = array (
                'FirstName' => $customer->getFirstname(),
                'LastName' => $customer->getLastname(),
                'Email' => $customer->getEmail(),
                'Phone' => $customer->getPhone(),
                'AccountId' => $account[0]->id
            );
            $sObject = new SObject();
            $sObject->fields = $customerFields;
            $sObject->type = 'Contact';
            $createResponse = $connection->create(array($sObject));
        } catch (Exception $e){
            Mage::log($e->getMessage());
        }

        return true;
    }

    /**
     * Function that updates the Contact after any Customer changes in Magento.
     * @param $customer
     * @return bool
     */
    public function updateCustomer($customer){
        $query = "SELECT * from Contact WHERE email = '" . $customer->getEmail() . "'";
        $customers = Mage::helper('scandi_sfbasic')->sfQuery($query);
        if (count($customers) == 1){
            $customerFields = array (
                'FirstName' => $customer->getFirstname(),
                'LastName' => $customer->getLastname(),
                'Email' => $customer->getEmail(),
                'Phone' => $customer->getPhone(),
                'Id' => $customers[0]->id
            );
            $sObject = new sObject();
            $connection = Mage::helper('scandi_sfbasic')->sfConnect();
            $createResponse = $connection->update(array($sObject), 'Lead');
        } else if (count($customers) == 0) {
            $this->exportCustomer($customer);
        } else {
            Mage::getSingleton('core/session')->addNotice('Something was wrong. Please check if the emails are unique.');
        }

        return true;
    }
}