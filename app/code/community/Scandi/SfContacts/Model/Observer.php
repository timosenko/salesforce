<?php
/**
 * @category    Scandi
 * @package    Scandi_SfContacts
 * @author    Matthew <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 */
class Scandi_SfContacts_Model_Observer extends Varien_Event_Observer
{
    /**
     * Simple observer which calls to the Helper
     * @param Varien_Event_Observer $observer
     * @return bool
     */
    public function customerExport(Varien_Event_Observer $observer){
        $customer = $observer->getEvent()->getCustomer();
        Mage::helper('scandi_sfcontacts')->updateCustomer($customer);
        return true;
    }
}