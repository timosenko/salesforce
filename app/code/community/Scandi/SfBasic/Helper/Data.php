<?php
/**
 * Basic integration element which possibly implements the connection function and interface.
 * @category Scandi
 * @package Scandi_SalesForce
 * @author Matthew <info@scandiweb.com>
 */
require_once(Mage::getBaseDir(). "/lib/salesforce/SforcePartnerClient.php");
class Scandi_SfBasic_Helper_Data extends Mage_Core_Helper_Abstract
{
    const SF_USERNAME = 'scandi_salesforce/connect/username';
    const SF_PASSWORD = 'scandi_salesforce/connect/password';
    const SF_TOKEN = 'scandi_salesforce/connect/token';

    /**
     * Basic connection function
     * @return connection client
     */
    public function sfConnect()
    {
        $password = Mage::getStoreConfig(self::SF_PASSWORD) . Mage::getStoreConfig(self::SF_TOKEN);
        $sforceConnection = new SforcePartnerClient();
        $sforceConnection->createConnection(Mage::getBaseDir()."/PartnerWSDL.xml");
        $sforceConnection->login(Mage::getStoreConfig(self::SF_USERNAME), $password);
        return $sforceConnection;
    }

    /**
     * Querying SalesForce - a method used just not to use default
     * @param $query
     * @return mixed
     */
    public function sfQuery($query)
    {
        $connection = $this->sfConnect();
        return $connection->query($query);
    }



}